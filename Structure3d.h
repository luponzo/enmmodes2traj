/*
 * Structure3d.h
 *
 *  Created on: Jul 3, 2012
 *      Author: gpolles
 */

#ifndef STRUCTURE3D_H_
#define STRUCTURE3D_H_

#include <vector>

#include "Vector3d.h"
#include "Bead.h"



class Structure3d
{

public:
  Structure3d(double cutoff);
  Structure3d(double cutoff, std::string fname);
  Structure3d(std::string fname);
  virtual ~Structure3d();

  void readFromPDBFile(const char* fname);
  void readFromPDBFile(std::string fname) {return readFromPDBFile(fname.c_str());}
  double    getDistance(size_t i, size_t j) const;
  double    getDistanceSQ(size_t i, size_t j) const;
  Vector3d  getDistanceVector(size_t i, size_t j);
  size_t    getSize() const {return _size;}
  void      updateNeighbors();
  std::vector<Bead>& getBeads() {
    return _beads;
  }
  Bead& getBead(size_t i){
    return _beads[i];
  }

private:
  std::vector<Bead> _beads;
  std::vector<Vector3d> _coordinates;
  std::vector<double>  _bFactors;
  size_t _size;
  double _cutoff;

};

#endif /* STRUCTURE3D_H_ */
