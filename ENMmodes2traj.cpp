//============================================================================
// Name        : ENMmodes2traj.cpp
// Author      : Guido Polles, modified by L.Ponzoni
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <cmath>
#include <boost/random.hpp>
#include "Structure3d.h"
#include "NormalModes.h"

using namespace std;

#define N_FRAMES 2000 /* NB: the first frame coordinates are the unperturbed ones */


int main(int argc, char* argv[]) {

  if(argc != 3 && argc != 4) {
    cerr << "  Usage: ./ENMmodes2traj <dir> <pdb_file> <#modes>" << endl;
    cerr << "     or: ./ENMmodes2traj <dir> <pdb_file>" << endl;
    cerr << "  where: <dir> = path to the directory containing the ENM eigenvectors" << endl;
    cerr << "         <pdb_file> = path to the pdb file" << endl;
    cerr << "         <#modes> = number of modes to be used" << endl;
    cerr << "                    (if unset, the maximum number is used instead)" << endl;

    return 1;
  }

  // check the directory path
  string dirPath = argv[1];
  if( dirPath.compare(dirPath.size()-1,1,"/") != 0 )
    dirPath.append("/");
  
  boost::mt19937 rng(time(0));

  Structure3d structure(argv[2]);

  int NMODES;
  int NMODES_MAX=structure.getSize()*3;
  if(argc == 4) {
    NMODES = std::atoi(argv[3]);
    if(NMODES > NMODES_MAX) NMODES=NMODES_MAX; 
  }
  else NMODES = NMODES_MAX;

  NormalModes nm;
  nm.readFromFile(dirPath, NMODES, structure.getSize(), false);
  vector<Vector3d> coords(structure.getSize());
  int chunk_size = structure.getSize()/100;

  FILE* fp = fopen("ENM_trajectory.xyz","w");
  
  
  // print the first (unperturbed) frame
  for (size_t i = 0; i < structure.getSize(); ++i){
    coords[i]=structure.getBead(i).getCoordinates();
    fprintf(fp,"%8.3lf %8.3lf %8.3lf\n", coords[i].X, coords[i].Y, coords[i].Z);
  }


  printf("Generating a %d-frames trajectory from %d normal modes.\n",N_FRAMES,NMODES);  

  
  for (size_t f = 0; f < N_FRAMES-1; ++f){
    for (size_t i = 0; i < structure.getSize(); ++i){
      coords[i]=structure.getBead(i).getCoordinates();
    }

    for (int m = 6; m < NMODES; ++m) {
      boost::normal_distribution<> nd(0.0, sqrt(1.0/nm.getEigenvalue(m)));
      boost::variate_generator<boost::mt19937&, boost::normal_distribution<> >  rnd(rng, nd);
      double width = rnd()/2.0;
//#pragma omp parallel for schedule(dynamic, chunk_size)
      for (size_t i = 0; i < structure.getSize(); ++i){
        coords[i] += nm.getEigenvector(m)[i]*width;
      }
    }

    //printf("%ld\ncoordinates generated from normal modes\n",structure.getSize());
    for (size_t i = 0; i < structure.getSize(); ++i){
      //printf("CA %8.3lf %8.3lf %8.3lf\n",
      fprintf(fp,"%8.3lf %8.3lf %8.3lf\n",
          coords[i].X,
          coords[i].Y,
          coords[i].Z);
    }

  }

  fclose(fp);  

  return 0;
}
