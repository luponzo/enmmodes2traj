/*
 * Structure3d.cpp
 *
 *  Created on: Jul 3, 2012
 *      Author: gpolles
 */


#define DEFAULT_CUTOFF 7.8 // default cutoff for neighbors search
#define CHAIN_BREAK_DIST 5.0
#define CHAIN_BREAK_DIST_SQ (CHAIN_BREAK_DIST*CHAIN_BREAK_DIST)

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "Structure3d.h"



Structure3d::Structure3d(std::string fname) : _size(0), _cutoff(DEFAULT_CUTOFF){
  readFromPDBFile(fname);
}



Structure3d::Structure3d(double cutoff = DEFAULT_CUTOFF) : _size(0),_cutoff(cutoff){
}
#undef DEFAULT_CUTOFF
Structure3d::Structure3d(double cutoff, std::string fname) : _size(0),_cutoff(cutoff){
  readFromPDBFile(fname);
}

Structure3d::~Structure3d() {
 }



void Structure3d::readFromPDBFile(const char* fname) {
  // For better performance, the vector capacity is preallocated
  // and expanded when necessary.
  const size_t expandSize = 1000; // size of expansion of capacity

  int discard_alternative_position=0;
  uint_t currentChainId = 0;
  char line[200], *a;
  char field[10];
  double x,y,z,bFactor;
  FILE *fp;

  /* PDB FORMAT

  COLUMNS        DATA TYPE     FIELD        DEFINITION
  -------------------------------------------------------------------------------------
   1 -  6        Record name   "ATOM  "

   7 - 11        Integer       serial       Atom serial number.

  13 - 16        Atom          name         Atom name.

  17             Character     altLoc       Alternate location indicator.

  18 - 20        Residue name  resName      Residue name.

  22             Character     chainID      Chain identifier.

  23 - 26        Integer       resSeq       Residue sequence number.

  27             AChar         iCode        Code for insertion of residues.

  31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.

  39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.

  47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.

  55 - 60        Real(6.2)    occupancy     Occupancy.

  61 - 66        Real(6.2)    tempFactor    Temperature factor.

  77 - 78        LString(2)   element       Element symbol, right-justified.

  79 - 80        LString(2)   charge        Charge on the atom.

   */

  if ((fp = fopen (fname, "r")) == NULL)
  {
    fprintf (stderr,"Could not open file %s.\n.Exiting.\n", fname);
    exit (1);
  }
  while (fgets(line, 200, fp)){
    if (strncmp (line, "ENDMDL", 6)==0){
      printf("Stopping at end of first model.\n");
      break;
    }
    /* check if line buffer begins with "ATOM" */
    if (!strncmp (line, "ATOM", 4)){
      a = line +12; /* advance line buffer and check if
                         we have a CA atom */
      /* check if character at position 16 is different from ' '
           or 'A'. If so, we are facing an alternative position
           and we will not consider it */
      discard_alternative_position=1;
      if (line[16]==' ') discard_alternative_position=0;
      if (line[16]=='A') discard_alternative_position=0;

      if (discard_alternative_position==1) continue;

      a = line +12; /* advance line buffer and check if
                                 we have a CA atom */
      if (!strncmp (a, " CA ", 4)){

        a = line +17;
        a=line+22;
        a=line+30;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&x)!=1){fprintf(stderr,"Fatal error. Could not read CA coordinate in protein %s.\nLine %s\n",fname,line); exit(1);}
        a=line+38;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&y)!=1){fprintf(stderr,"Fatal error. Could not read CA coordinate in protein %s.\nLine %s\n",fname,line); exit(1);}
        a=line+46;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&z)!=1){fprintf(stderr,"Fatal error. Could not read CA coordinate in protein %s.\nLine %s\n",fname,line); exit(1);}


        /* Finally read B factor */
        a= line + 60;
        strncpy(field,a,6);field[6]='\0';
        if ( sscanf(field,"%lf",&bFactor)!=1)  {bFactor=0.0; }

        /* check capacity */
        if(_beads.capacity() <= _beads.size()) _beads.reserve(_beads.capacity() + expandSize);


        /* add record */
        _beads.push_back(Bead());
        _beads.back().setCoordinates(Vector3d(x,y,z));
        _beads.back().setBetaFactor(bFactor);
        _beads.back().setPdbPosition(_size);
        _beads.back().setIndex(_size);
        ++_size;

        if(_size>1){
          if(_beads[_size-1].distanceSQ(_beads[_size-2])>CHAIN_BREAK_DIST_SQ){
            ++currentChainId;
          }
        }
        _beads.back().setChainId(currentChainId);

      }
    }
  }
  updateNeighbors();

}



double Structure3d::getDistance(size_t i, size_t j) const{
  assert(i<_size && j < _size);
  return _beads[i].getCoordinates().distance(_beads[j].getCoordinates());
}



Vector3d Structure3d::getDistanceVector(size_t i, size_t j) {
  return _beads[j].getCoordinates() - _beads[i].getCoordinates();
}

double Structure3d::getDistanceSQ(size_t i, size_t j) const {
  assert(i<_size && j < _size);
  return _beads[i].getCoordinates().distanceSQ(_beads[j].getCoordinates());
}



void Structure3d::updateNeighbors() {

  _size = _beads.size();
  double cutoffSQ = _cutoff*_cutoff;

  for (size_t i = 0; i < _size; ++i) {
      for (size_t j = i+1; j < _size; ++j){
        if(_beads[j].getCoordinates().distanceSQ(_beads[i].getCoordinates())< cutoffSQ){
          _beads[i].addNeighbor(&_beads[j]);
          _beads[j].addNeighbor(&_beads[i]);
        }
      }
    }
}

