/*
 * Bead.h
 *
 *  Created on: Jul 19, 2012
 *      Author: gpolles
 *
 *  Bead class.
 *  Each bead has 3 coordinates and pointers to its neighbors
 *  and to a domain.
 *  It have also some information from the pdb file
 *  as chainId, and betaFactor
 *
 */

#ifndef BEAD_H_
#define BEAD_H_

#include <list>

#include "Vector3d.h"
#include "common.h"





class RigidDomain;


class Bead
{
public:
  Bead() : _index(0), _num_neighbors(0), _domain(NULL),
           _isEdge(false), _coordinates(Vector3d(0,0,0)),
           _betaFactor(0.0), _pdbPosition(0), _chainId(0) {}
  virtual ~Bead(){}

  double distance(Bead* b){
    return _coordinates.distance(b->_coordinates);
  }

  double distance(Bead& b){
    return _coordinates.distance(b._coordinates);
  }

  double distanceSQ(Bead* b){
    return _coordinates.distanceSQ(b->_coordinates);
  }

  double distanceSQ(Bead& b){
    return _coordinates.distanceSQ(b._coordinates);
  }

  RigidDomain* getDomain() const {
    return _domain;
  }

  void setDomain(RigidDomain* domain){
    _domain = domain;
  }

  uint_t getIndex() const {
    return _index;
  }

  void setIndex(uint_t index) {
    _index = index;
  }

  std::list<Bead*>& getNeighbors() {
    return _neighbors;
  }

  void setNeighbors(std::list<Bead*> neighbors) {
    _neighbors = neighbors;
  }

  uint_t getNumNeighbors() const {
    return _num_neighbors;
  }

  void setNumNeighbors(uint_t numNeighbors) {
    _num_neighbors = numNeighbors;
  }

  void addNeighbor(Bead* neighbor){
    _neighbors.push_back(neighbor);
    _num_neighbors = _neighbors.size();
  }

  bool isEdge(){
    return _isEdge;
  }

  void setEdge(bool b){
    _isEdge =b;
  }

  double getBetaFactor() const {
    return _betaFactor;
  }

  void setBetaFactor(double betaFactor) {
    _betaFactor = betaFactor;
  }

  const Vector3d& getCoordinates() const {
    return _coordinates;
  }

  void setCoordinates(const Vector3d& coordinates) {
    _coordinates = coordinates;
  }

  int getPdbPosition() const {
    return _pdbPosition;
  }

  void setPdbPosition(uint_t pdbPosition) {
    _pdbPosition = pdbPosition;
  }

  uint_t getChainId() const {
    return _chainId;
  }

  void setChainId(uint_t chainId) {
    _chainId = chainId;
  }

private:
  uint_t _index;
  uint_t _num_neighbors;
  RigidDomain* _domain;
  std::list<Bead*> _neighbors;
  bool _isEdge;
  Vector3d _coordinates;
  real_t _betaFactor;
  uint_t _pdbPosition;
  uint_t _chainId;
};

#endif /* BEAD_H_ */
