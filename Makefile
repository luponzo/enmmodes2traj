ifndef BOOST_LIB_PATH 
BOOST_LIB_PATH = '/u/sbp/lponzoni/Programs/boost_1_57_0'
endif 

# if -static doesn't work, the static libraries are not present 

all:
	g++  -O3 ENMmodes2traj.cpp NormalModes.cpp Structure3d.cpp -I${BOOST_LIB_PATH} -o ENMmodes2traj
